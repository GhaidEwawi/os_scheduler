# include "Headers.h"

void inputFromFile(Data& data, string fileName) {
	fstream input;
	input.open(fileName);

	input >> data.memorySize;
	input >> data.pageSize;
	input >> data.rrQ;
	input >> data.contextSwitch;

	for (int i = 0; i < data.getNumberOfProcesses(); i++) {
		input >> data.processes[i].id >> data.processes[i].time >> data.processes[i].burst >> data.processes[i].size;
	}

	input.close();
}

AllCalculations computeFCFS(const Data& data) {
	AllCalculations result("FCFS");
	copyIds(result, data);
	calculateWaitFCFS(result, data);
	calculateTurnaroundFCFS(result, data);
	calculateFinishFCFS(result, data);

	return result;
}

void copyIds(AllCalculations& result, const Data& data) {
	for (int i = 0; i < NUMBER_OF_PROCESSES; i++) {
		result.times[i].id = data.processes[i].id;
	}
}

void copyBurst(int remaining[], const Data& data) {
	for (int i = 0; i < NUMBER_OF_PROCESSES; i++) {
		remaining[i] = data.processes[i].burst;
	}
}

void calculateWaitFCFS(AllCalculations& result, const Data& data) {
	for (int i = 0; i < NUMBER_OF_PROCESSES; i++) {
		if (i == 0) {
			result.times[i].wait = data.processes[i].time;
			continue;
		}
		int temp = 0;
		for (int j = 0; j < i; j++) {
			temp += (data.processes[j].burst + data.contextSwitch);
		}
		result.times[i].wait =  temp - data.processes[i].time;
	}
}

void calculateTurnaroundFCFS(AllCalculations& result, const Data& data) {
	for (int i = 0; i < NUMBER_OF_PROCESSES; i++) {
		result.times[i].turnaround = result.times[i].wait + data.processes[i].burst;
	}
}

void calculateFinishFCFS(AllCalculations& result, const Data& data) {
	for (int i = 0; i < NUMBER_OF_PROCESSES; i++) {
		if (i == 0)
			result.times[i].finish = data.processes[i].burst;
		else
			result.times[i].finish = data.contextSwitch + result.times[i - 1].finish + data.processes[i].burst;
	}
}

AllCalculations computeSJF(Data& data) {
	AllCalculations result("SJF");

	calculateAllTimes(result, data);
	copyIds(result, data);

	return result;
}

void calculateAllTimes(AllCalculations& result, Data& data) {
	result.times[0].finish = data.processes[0].burst;
	result.times[0].wait = data.processes[0].time;
	result.times[0].turnaround = data.processes[0].burst;
	for (int i = 1; i < NUMBER_OF_PROCESSES; i++) {
		data.sortBurstData(i);
		result.times[i].wait = data.contextSwitch + result.times[i - 1].finish - data.processes[i].time;
		result.times[i].finish = data.contextSwitch + result.times[i - 1].finish + data.processes[i].burst;
		result.times[i].turnaround = result.times[i].finish - data.processes[i].time;
	}
}

void printArray(const int arr[]) {
	int size = sizeof(arr) + 1;
	for (int i = 0; i < size; i++) {
		cout << i + 1 << " " << arr[i] << endl;
	}
}
AllCalculations computeRR(const Data& data) {
 	AllCalculations result("RR");
	int remainingTime[NUMBER_OF_PROCESSES];
	int numberOfActiveProcesses = NUMBER_OF_PROCESSES;

	copyBurst(remainingTime, data);
	copyIds(result, data);

	/// For the first process only
	int currentTime = 0;//min(data.rrQ, data.processes[0].burst);


	int roundNumber = 1;
	int lastExcuted = 5;
	int ctr = 0;
	
	while (numberOfActiveProcesses) {

		for (int i = 0; i < NUMBER_OF_PROCESSES && numberOfActiveProcesses; i++) {
		
			if (ctr >= NUMBER_OF_PROCESSES) {
				ctr = 0;
				currentTime++;	
			}
			if (remainingTime[i] == 0) {
				continue;
			}
			if (!(i == 0 && roundNumber == 1) && data.processes[i].time > currentTime - 2 && remainingTime[i] != 0) {
				ctr++;
				break;
			}
			
			if (lastExcuted == i)
				currentTime -= data.contextSwitch;

			if (remainingTime[i] <= data.rrQ) {

				if (lastExcuted != i) {
					if (roundNumber == 1)
						result.times[i].wait = currentTime - data.processes[i].time;
					else
						result.times[i].wait = result.times[i].wait + currentTime - (data.processes[i].time);
				}
				else {
					result.times[i].wait = currentTime - data.rrQ - data.processes[i].time;
				}
				if (roundNumber == 1 && i == 0)
					result.times[i].wait = 0;

				currentTime += remainingTime[i];
				remainingTime[i] -= remainingTime[i];
	
				result.times[i].finish = currentTime;
				numberOfActiveProcesses--;

			}
			else {
				remainingTime[i] -= data.rrQ;
				currentTime += data.rrQ;
			}




			currentTime += data.contextSwitch;
			lastExcuted = i;

		}
		roundNumber++;
	}

	calculateTurnaroundRR(result, data);

	return result;
}

void calculateTurnaroundRR(AllCalculations& result, const Data& data) {
	for (int i = 0; i < NUMBER_OF_PROCESSES; i++) {
		result.times[i].turnaround = result.times[i].finish - data.processes[i].time;
	}
}
int Data::getNumberOfProcesses() {
	return NUMBER_OF_PROCESSES;
}

bool sortProcessByArrival(Process one, Process two) {
	return (one.time < two.time);
}

bool sortProcessByBurst(Process one, Process two) {
	return (one.burst < two.burst) ;
}