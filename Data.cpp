# include "Process.h"
# include "Headers.h"

void Data::sortArrivalData() {
	sort(&processes[0], &processes[NUMBER_OF_PROCESSES], sortProcessByArrival);
}

void Data::sortBurstData(int i) {
	sort(&processes[i], &processes[NUMBER_OF_PROCESSES], sortProcessByBurst);
}

void Data::print() {
	cout << memorySize << endl;
	cout << pageSize << endl;
	cout << rrQ << endl;
	cout << contextSwitch << endl;
	for (int i = 0; i < getNumberOfProcesses(); i++) {
		processes[i].print();
		cout << endl;
	}
}