#pragma once
# include "Data.h"

/// struct ProcessTime, contains the id of a process along with its finish, wait and turnaround time.
struct ProcessTime {
	/// process id
	int id;
	/// process finish time
	int finish;
	/// process wait time
	int wait;
	/// process turnaround time
	int turnaround;
};

/// Copies burst time from a Data struct to an array that has the same length as the number of processes
void copyBurst(int remaining[], const Data& data);

/// Contains data regarding the algorithm name, cpu Utilization, average wait, turnaround, finish times. 
/// Also contains all processes id and their wait, turnaround and finish time
struct AllCalculations {
	/// The name of the algorithm
	string algorithmName;
	/// The calculated average of finish times of all processes
	double avgFinish;
	/// The calculated average of wait times of all processes
	double avgWait;
	/// The calculated average of turnaround times of all processes
	double avgTurnaround;
	/// The calculated cpu Utilization from the processes
	double cpuUtilization;
	ProcessTime times[NUMBER_OF_PROCESSES];

	/// Prints all member data stored in the struct
	void print();
	/// Initializes the algorithm name used in the calculations.
	AllCalculations(string name = "");
	/// Draws and prints the GanttChart from the array of processes.
	void drawGantt(const Data& data);
	/// Draws and prints the GanttChart of Round Robin Algorithm.
	void drawGantt(const Data& data, int quanta);
	/// Calculate the average wait, turnaround, finish times from the array of all processes times.
	void calculateAverages();
	/// Calculates the cpu Utilization and saves it into the member cpuUtilization
	void calculateCpuUtilization(const Data& data);
};
