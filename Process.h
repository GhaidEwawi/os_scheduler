#pragma once

#include "ExternalLibraries.h"

/// Contains the processe's details. Such as, id, arrival time, burst duration, size
struct Process {
	/// The id of the process
	int id;
	/// The arrival time of the process
	int time;
	/// Burst duration of the process
	int burst;
	/// The size of the process
	int size;

	Process(int tId = 0, int tTime = 0, int tBurst = 0, int tSize = 0);

	/// Prints the process id, arrival time, burst time and size
	void print();
};

/// returns true if arrival time of the first parameter is less than arrival time of second 
bool sortProcessByArrival(Process one, Process two);
/// returns true if arrival time of the first parameter is less than burst time of second 
bool sortProcessByBurst(Process one, Process two);