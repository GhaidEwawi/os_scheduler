#pragma once

#include "AllCalculations.h"

/// Reads data from a file and stores in Data struct type
void inputFromFile(Data& data, string fileName);

/// Copies all processes ids from Data struct into AllCalculations Struct
void copyIds(AllCalculations& result, const Data& data);

/// Calcualtes wait time for FCFS algorithm
void calculateWaitFCFS(AllCalculations& result, const Data& data);

/// Calcualtes turnaround time for FCFS algorithm
void calculateTurnaroundFCFS(AllCalculations& result, const Data& data);

/// Calcualtes finish time for FCFS algorithm
void calculateFinishFCFS(AllCalculations& result, const Data& data);

/// Returns result of type AllCalculations that contains calculated wait, turnaround and finish time
/// for all processes using FCFS algorithm
AllCalculations computeFCFS(const Data& data);

/// A function used be SJF algorithm
void calculateAllTimes(AllCalculations& result, Data& data);
/// Returns result of type AllCalculations that contains calculated wait, turnaround and finish time
/// for all processes using SJF algorithm
AllCalculations computeSJF(Data& data);

/// A function used by round robin algorithm
void calculateTurnaroundRR(AllCalculations& result, const Data& data);
/// Returns result of type AllCalculations that contains calculated wait, turnaround and finish time
/// for all processes using Round Robin algorithm
AllCalculations computeRR(const Data& data);