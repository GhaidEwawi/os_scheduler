#pragma once
# include "Process.h"

/// Contains the simulation data. Such as: memory size, page size, round robin quantum, context switch duration, array of processes
struct Data {
	/// The simulation's memory size
	int memorySize;
	/// The simulation's page size
	int pageSize;
	/// The size of round robin quantum
	int rrQ;
	/// The duration of the conext switch.
	int contextSwitch;
	/// An array that contains the information of each process
	Process processes[NUMBER_OF_PROCESSES];

	/// Return an int the represents the number of processes
	int getNumberOfProcesses();

	/// Prints all member details of the stuct
	void print();

	/// Sorts the processes inside the array based on their arrival time
	void sortArrivalData();
	/// Sorts the processes inside the array based on their burst time, beginning from the defined index
	void sortBurstData(int i);
};