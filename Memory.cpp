#include "Memory.h"

Memory::Memory(int frame, int s) {
	frameSize = frame;
	size = s;
	numberOfFrames = ceil(size / frameSize);
	processId.assign(numberOfFrames, -1);
	pageNumber.assign(numberOfFrames, -1);
	srand(time(0));
}

int Memory::getNumberOfFrames() {
	return numberOfFrames;
}

int Memory::getEmptyFrame() {
	int size = getNumberOfFrames();
	int randomFrame;
	while (true) {
		randomFrame = (rand() % size);
		if (processId[randomFrame] == -1)
			return randomFrame;
	}
}

void Memory::setProcessToFrame(int processID, int processPage, int _frameNumber) {
	processId[_frameNumber] = processID;
	pageNumber[_frameNumber] = processPage;

}

void Memory::print() {
	cout << setfill(' ') << setw(SPACE + 6) << "" << setfill('-') << setw(SPACE) << "" << endl;
	size = processId.size();
	for (int i = 0; i < size; i++) {
		if (processId[i] == -1)
			cout << setfill(' ') << setw(SPACE - 1) << "" << setw(3) << i << setw(3) << "" << "|" <<
			setw(SPACE-5) << "EMPTY" << setw(5) << "" << "|" << endl;
		else {
			cout << setfill(' ')<< setw(SPACE - 1 ) << "" << setw(3) << i << setw(3) << "" << "|" <<
				setw(SPACE - 7) << processId[i] << setw(7) << "" << "|" << endl;
		}
		cout << setfill(' ') << setw(SPACE + 6) << "" << setfill('-') << setw(SPACE) << "" << endl;
	}
}