# include "AllCalculations.h"

void AllCalculations::calculateAverages() {
	double tempAvgFinish = 0, tempAvgWait = 0, tempAvgTurnaround = 0;
	for (int i = 0; i < NUMBER_OF_PROCESSES; i++) {
		tempAvgFinish += times[i].finish;
		tempAvgWait += times[i].wait;
		tempAvgTurnaround += times[i].turnaround;
	}
	avgFinish = tempAvgFinish / 5.0;
	avgWait = tempAvgWait / 5.0;
	avgTurnaround = tempAvgTurnaround / 5.0;

}

void AllCalculations::drawGantt(const Data& data, int quanta) {

	int numberOFDashes = times[NUMBER_OF_PROCESSES - 1].finish + 2 * (NUMBER_OF_PROCESSES - 1);
	cout << " ";
	cout << setfill('-') << setw(numberOFDashes) << "" << endl << "|";

	int remainingTime[NUMBER_OF_PROCESSES];
	copyBurst(remainingTime, data);
	int activeProcesses = NUMBER_OF_PROCESSES;
	int currentTime = 0;

	while (activeProcesses) {
		for (int i = 0; i < NUMBER_OF_PROCESSES && activeProcesses; i++) {

			if (remainingTime[i] == 0)
				continue;
			if (data.processes[i].time > currentTime) {
				break;
			}

			currentTime += min(remainingTime[i], data.rrQ);
			const int temp = min(remainingTime[i], data.rrQ);
			remainingTime[i] = max(remainingTime[i] - data.rrQ, 0);
			if (remainingTime[i] == 0)
				activeProcesses--;

			if (temp % 2 == 0) {
				cout << setfill(' ') << setw(temp / 2) << times[i].id;
			}
			else {
				cout << setfill(' ') << setw((temp / 2) + 1) << times[i].id;
			}

			if (i == NUMBER_OF_PROCESSES - 1)
				cout << setw(temp / 2) << "" << "|";
			else
				cout << setw(temp / 2) << "" << "|" << setw(data.contextSwitch) << "" << "|";
			currentTime += data.contextSwitch;
		}
	}

	cout << endl << " ";
	cout << setfill('-') << setw(numberOFDashes) << "" << endl;

}

void AllCalculations::drawGantt(const Data& data) {
	int numberOFDashes = times[NUMBER_OF_PROCESSES - 1].finish + 2 * (NUMBER_OF_PROCESSES - 1);
	cout << " ";
	cout << setfill('-') << setw(numberOFDashes) << "" << endl << "|";

	for (int i = 0; i < NUMBER_OF_PROCESSES; i++) {
		const int temp = data.processes[i].burst;
		if (temp % 2 == 0) {
			cout << setfill(' ') << setw(temp / 2) << times[i].id;
		}
		else {
			cout << setfill(' ') << setw((temp / 2) + 1) << times[i].id;
		}
		if (i == NUMBER_OF_PROCESSES - 1)
			cout << setw(temp / 2) << "" << "|";
		else
			cout << setw(temp / 2) << "" << "|" << setw(data.contextSwitch) << "" << "|";

	}

	cout << endl << " ";
	cout << setfill('-') << setw(numberOFDashes) << "" << endl;
	cout << " ";
	for (int i = 0; i < NUMBER_OF_PROCESSES; i++) {
		const int temp = data.processes[i].burst;
		cout << setfill(' ') << setw(temp+1) << times[i].finish;

		if (i == NUMBER_OF_PROCESSES - 1)
			cout << setw(temp / 2) << "";
		else if(data.contextSwitch > 0)
			cout << setw(data.contextSwitch+1) << times[i].finish + data.contextSwitch;

	}

}

AllCalculations::AllCalculations(string name) {
	algorithmName = name;
	int temp = sizeof(times) / sizeof(times[0]);
	for (int i = 0; i < temp; i++) {
		times[i].finish = 0;
		times[i].wait = 0;
		times[i].turnaround = 0;
	}
}

void AllCalculations::calculateCpuUtilization(const Data& data) {
	if (data.contextSwitch == 0) {
		cpuUtilization = 1;
		return;
	}
	double finishTime = times[NUMBER_OF_PROCESSES - 1].finish;
	if(algorithmName != "RR")
		cpuUtilization = (finishTime - data.contextSwitch * (NUMBER_OF_PROCESSES - 1)) / finishTime;
	else {
		finishTime = max(times[0].finish,
					max(times[1].finish,max(times[2].finish,max(times[3].finish, times[4].finish))));
		int bursts = 0;
		for (int i = 0; i < NUMBER_OF_PROCESSES; i++) {
			cout << "First " << data.processes[i].burst << " " << data.rrQ << endl;
			if ((data.processes[i].burst % data.contextSwitch) == 0) {
			bursts += (data.processes[i].burst / data.rrQ);
			cout << (data.processes[i].burst / data.rrQ) << endl;
			}
			else {
				bursts += (data.processes[i].burst / data.rrQ) + 1;
				cout << (data.processes[i].burst / data.rrQ) + 1 << endl;

			}

		}
		cpuUtilization = (finishTime - data.contextSwitch * (bursts - 3)) / finishTime;
	}
}

void AllCalculations::print() {
	calculateAverages();
	cout << setfill(' ') << endl;
	cout << setw(SPACE) << "" << "The used algorithm is " << algorithmName << endl << endl;
	cout << setw(SPACE) << "" << "Average Finish Time :    \t" << avgFinish << endl;
	cout << setw(SPACE) << "" << "Average Wait Time :      \t" << avgWait << endl;
	cout << setw(SPACE) << "" << "Average Turnaround Time : \t" << avgTurnaround << endl;
	cout << setw(SPACE) << "" << "CPU Utilization : \t\t" << cpuUtilization << endl << endl;
	cout << setw(SPACE) << "Process id" << setw(SPACE) << "Finish" << setw(SPACE) << "Wait" << setw(SPACE) << "Turnaround" << endl;
	for (int i = 0; i < NUMBER_OF_PROCESSES; i++) {
		cout << setw(SPACE) << times[i].id << setw(SPACE) << times[i].finish << setw(SPACE) << times[i].wait << setw(SPACE) << times[i].turnaround << endl;
	}
}