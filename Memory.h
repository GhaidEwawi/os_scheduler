#pragma once
#include "ExternalLibraries.h"
#include "Data.h"


class Memory {
private:
	int frameSize;
	int size;
	vector <int> processId;
	vector <int> pageNumber;
	int numberOfFrames;

public:
	Memory(int frame, int s);
	int getNumberOfFrames();
	int getEmptyFrame();
	void setProcessToFrame(int processID, int processFrame, int _frameNumber);
	void print();
};