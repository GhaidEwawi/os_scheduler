# include "Headers.h"
# include "Memory.h"

int frame[NUMBER_OF_PROCESSES][30]; int page[NUMBER_OF_PROCESSES][30];


void printFrames(const Data& data, Memory& mem) {
	for (int i = 0; i < NUMBER_OF_PROCESSES; i++) {
		int memoryFrames = ceil(data.processes[i].size / data.pageSize);
		cout << endl;
		cout << setfill(' ') << setw(SPACE) << "    Process Number " << data.processes[i].id << ". # of frames = " << memoryFrames << endl;
		cout << setw(SPACE) << "Page" << setw(SPACE) << "Frame" << endl;
		for (int j = 0; j < memoryFrames; j++) {

			cout << setw(SPACE) << page[i][j] << setw(SPACE) << frame[i][j] << endl;
		}
	}
}

/// The result of this function is the mapping of each page of a process to a frame in the main memory.
/// Frames are all allocated and stored in the class called Memory.
void mapPagesToFrames(const Data& data, Memory& mem) {
	for (int i = 0; i < NUMBER_OF_PROCESSES; i++) {
		int memoryFrames = ceil(data.processes[i].size / data.pageSize);
		for (int j = 0; j < memoryFrames; j++) {
			page[i][j] = j;
			frame[i][j] = mem.getEmptyFrame();
			mem.setProcessToFrame(data.processes[i].id, j, frame[i][j]);
		}
	}
	printFrames(data, mem);
}
int main() {
	Data data;
	inputFromFile(data, "test2.txt");
	data.print();
	/*
	AllCalculations result;
	data.sortArrivalData();
	cout << "Data " << endl;
	data.print();
	Data data2 = data;
	data2.print();
	result = computeRR(data);
	result.calculateCpuUtilization(data);
	result.print();
	result.drawGantt(data);
	*/
	/*
	AllCalculations result2 = computeSJF(data2);
	result2.print();
	result2.drawGantt(data2);

	AllCalculations result3 = computeRR(data);
	data.print();
	result3.print();
	result3.drawGantt(data, 10);*/
	
	Memory mem(data.pageSize, data.memorySize);

	mapPagesToFrames(data, mem);
	mem.print();



	int pNo, logicalAddress;
	cout << "Enter the process number : ";
	cin >> pNo;
	cout << "Enter the logical address : ";
	cin >> logicalAddress;
	int offset = logicalAddress - (logicalAddress / data.pageSize) * data.pageSize;
	cout << logicalAddress << " --> (p, d) = (" << int(logicalAddress / data.pageSize) << ", " <<
		offset << ") --> (f, d) = (" << frame[pNo][max(0, (logicalAddress / data.pageSize) - 1)] << ", " <<
		offset << ") --> " << offset + (frame[pNo][max(0,(logicalAddress / data.pageSize) - 1)] * data.pageSize) << endl;

		
	return 0;
}

