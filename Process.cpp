# include "Process.h"

using namespace std;

Process::Process(int tId, int tTime, int tBurst, int tSize) {
	id = tId;
	time = tTime;
	burst = tBurst;
	size = tSize;
}

void Process::print() {
	cout << id << '\t' << time << '\t' << burst << '\t' << size;
}